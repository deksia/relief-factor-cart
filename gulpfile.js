const gulp = require('gulp');

const sourcemaps = require('gulp-sourcemaps');
const imagemin = require('gulp-imagemin');
const del = require('del');
const browserSync = require('browser-sync').create();

const autoprefixer = require('gulp-autoprefixer');
const sass = require('gulp-sass');
const cssnano = require('gulp-cssnano');
const hash = require('gulp-hash-filename');

const compiler = require('webpack');
const webpack = require('webpack-stream');
const webpackConfig = require('./webpack.config');

// css
gulp.task('clean-css', () => {
	return del(['public/css/**/*']);
});

gulp.task(
	'css-dev',
	gulp.series('clean-css', () => {
		return gulp
			.src('src/scss/*.scss')
			.pipe(
				hash({
					format: '{name}{ext}'
				})
			)
			.pipe(sourcemaps.init())
			.pipe(sass())
			.pipe(autoprefixer('last 1 version'))
			.pipe(cssnano())
			.pipe(sourcemaps.write('.'))
			.pipe(gulp.dest('public/css'))
			.pipe(browserSync.stream());
	})
);

gulp.task(
	'css-prod',
	gulp.series('clean-css', () => {
		return gulp
			.src('src/scss/*.scss')
			.pipe(
				hash({
					format: '{name}{ext}'
				})
			)
			.pipe(sass())
			.pipe(autoprefixer('last 1 version'))
			.pipe(cssnano())
			.pipe(gulp.dest('public/css'));
	})
);

// js
gulp.task('clean-js', () => {
	return del(['public/js/**/*']);
});

gulp.task(
	'js-dev',
	gulp.series('clean-js', () => {
		return gulp
			.src('src/js/*.js')
			.pipe(
				webpack(
					{
						...webpackConfig,
						mode: 'development'
					},
					compiler
				)
			)
			.pipe(gulp.dest('public/js'))
			.pipe(browserSync.stream());
	})
);

gulp.task(
	'js-prod',
	gulp.series('clean-js', () => {
		return gulp
			.src('src/js/*.js')
			.pipe(
				webpack(
					{
						...webpackConfig,
						mode: 'production'
					},
					compiler
				)
			)
			.pipe(gulp.dest('public/js'));
	})
);

// Helper function to allow browser reload with Gulp 4.
const reload = done => {
	browserSync.reload();
	done();
};


// media
gulp.task('img-dev', () => {
	return gulp.src('src/images/**/*').pipe(gulp.dest('public/images'));
});

gulp.task('img-prod', () => {
	return gulp
		.src('src/images/**/*')
		.pipe(imagemin())
		.pipe(gulp.dest('public/images'));
});


// meta
gulp.task('build-dev', gulp.parallel('css-dev', 'js-dev', 'img-dev'));
gulp.task('build', gulp.parallel('css-prod', 'js-prod', 'img-prod'));

gulp.task('watch', () => {
	browserSync.init({
        proxy: "http://relieffactor.local"
    });
 
	gulp.watch('src/scss/**/*.scss', gulp.parallel('css-dev'));
	gulp.watch('src/js/**/*.js', gulp.parallel('js-dev'));
	gulp.watch('src/images/**/*', gulp.parallel('img-dev'));
	gulp.watch("./**/*.php", gulp.parallel(reload)); 
	gulp.watch("./**/*.html", gulp.parallel(reload)); 

});

gulp.task('default', gulp.series('build-dev', 'watch'));
