# DEKSIA STARTERKIT

### Installation

Install the dependencies and devDependencies and start the server.

```sh
$ cd starterkit
$ npm install
```
### Live compiling
```sh
$ npm start

### Compile build
```sh
$ gulp build
```