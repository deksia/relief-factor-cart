# DEKSIA STARTERKIT


### Install the dependencies and devDependencies and start the server.
```sh
$ npm install
```

### Open the gulpfile.js and change "YOURSITEHERE.local" to your local dev domain


### Run NPM Start (this should start browser sync)
```sh
$ npm start
```

### **Run production build before deploying!**
```sh
$ gulp build
```
