// import { mmenuStarter } from './mmenu.js';
import $ from 'jquery';

$(document).ready(function() {
	$('.js-checkout-panel button').click(function(e) {
		e.preventDefault();
		console.log(this);
		var parentPanel = $(this).parents('.js-checkout-panel');
		$(parentPanel).find('.c-checkout-panel__inner').slideUp();
		$(parentPanel).next().find('.c-checkout-panel__inner').slideDown();
		$(parentPanel).removeClass('c-checkout-panel--active');
		$(parentPanel).next().addClass('c-checkout-panel--active');
		var nextPanelIndex = $(parentPanel).index() + 1;

		$('.c-checkout-tabs li').removeClass('active');
		$('.c-checkout-tabs').find('li').eq(nextPanelIndex).addClass('active');

	})

	$('.c-checkout-tabs li').click(function(e) {
		$('.c-checkout-tabs li').removeClass('active');
		$(this).addClass('active');
		var thisPanel = $(this).index();
		$('.c-checkout-panel__inner').slideUp();
		$('.js-checkout-panel').eq(thisPanel).find('.c-checkout-panel__inner').slideDown();
		$('.js-checkout-panel').removeClass('c-checkout-panel--active');
		$('.js-checkout-panel').eq(thisPanel).addClass('c-checkout-panel--active');

	})
});
