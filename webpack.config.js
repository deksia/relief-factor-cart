const path = require('path');
const TerserPlugin = require('terser-webpack-plugin');

module.exports = {
	devtool: 'source-map',
	output: { filename: '[name].js' },
	module: {
		rules: [
			{
				test: /\.(js|jsx)$/,
				exclude: /node_modules/,
				include: path.resolve(__dirname, 'src', 'js'),
				loader: 'babel-loader'
			}
		]
	},
	optimization: {
		minimizer: [new TerserPlugin({ parallel: true })]
	}
};
